# Prerequisite

This is prerequisite for SAS Viya Supplementary Training Lab.
* You should have Access of Azure Subscription for creating kubernetes cluster. Please check if you have access to "sas-gelsandbox" subscription. If not then follow below steps
   * Send this [email](mailto:dlmanager@wnt.sas.com?subject=SUBSCRIBE%20azure_gelsandbox) without any change.
   * If the link above does not open your default email program then simply email dlmanager@wnt.sas.com with subject "SUBSCRIBE azure_gelsandbox".
   * DLManager will send an email response in a few minutes confirming your membership in the GELSANDBOX Azure subscription.
   * As said in the email you will have to wait for about **15 minutes** (for the AD replication to propagate the change) before continuing with the prerequisite steps.

* Please make yourself available with Ubuntu environment to connect to Azure and Kubernetes. I have made use of WSL version 1, which is having Ubuntu. If you don't have WSL then can get configured using below link - https://docs.microsoft.com/en-us/windows/wsl/install-win10#simplified-installation-for-windows-insiders.

* In your Ubuntu Environment you make below required setup
   - AzureCLI - Reference Command
   ```sh
   sudo apt install software-properties-common
   sudo add-apt-repository ppa:deadsnakes/ppa
   sudo apt install python3.7
   sudo apt-get install -y libffi-dev
   sudo apt-get install libssl-dev
   curl -L https://aka.ms/InstallAzureCli | bash
   ```
   - Git
   ```sh
   sudo apt install git
   ```
   - kubectl
   ```sh
   curl -LO "https://dl.k8s.io/release/v1.19.9/bin/linux/amd64/kubectl"
   curl -LO "https://dl.k8s.io/v1.19.9/bin/linux/amd64/kubectl.sha256"
   echo "$(<kubectl.sha256) kubectl" | sha256sum --check
   sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
   ```
* Execute below command to check if you are able to connect to Azure Subscription. Before executing please modify resolve.conf to add 0.0.0.0 if you are using WSL.
  ```sh 
   sudo nano /etc/resolv.conf
   -- add nameserver 0.0.0.0
  ```
  ```sh
   az login --use-device-code
   az account set -s "sas-gelsandbox"
   az configure --defaults location=eastus
  ```
* Check if you have Git Installed
   ```sh
   git --version
   ```
* Check if you have kubectl Installed properly
   ```sh
   kubectl--version
   ```
* Check if you have gitlab.sas.com access from WSL (Ubuntu)
   ```sh
   nslookup gitlab.sas.com
   ```
